# from bottle import route
from bottle import run, route, view, get, static_file, post, request


@route("/")
@view("fronteira")
def home():
    return {}


@get("/prototipo.py")
def getpy():
    return static_file("prototipo.py", root=".")


@get("/wbox.png")
def getpy():
    return static_file("wbox.png", root=".")


@post("/register")
def post_action(**_):
    x, y, t = request.POST["x"], request.POST["y"], request.POST["t"]
    print("postou", x, y, t)


if __name__ == '__main__':
    run()
