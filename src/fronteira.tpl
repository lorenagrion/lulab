<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta http-equiv="content-type" content="application/xml;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.8.0/p5.js"></script>
    <!--script type="text/javascript" src="https://j.mp/brython_371"></script -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/brython/3.4.0/brython.min.js">
    </script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/brython/3.4.0/brython_stdlib.js">
    </script>
    <script type="text/python">
            from prototipo import main
            from browser import document, html, ajax, window
            class Brython:
                doc = document
                html = html
                ajax = ajax
                @staticmethod
                def timestamp():
                    return window.Date.now() / 1000
            main(Brython)

    </script>
    <style>
        /* #9E7681,#E4E2D7,#FA089B,#EC7C9C,#2F3F53,#9E7681,#E4E2D7,#FA089B */
        body
        {
          background-color: #052233;
        }
        .center-div
        {
          position: absolute;
          margin: auto;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          width: 900px;
          height: 650px;
          background-color: #9E7681;
          border-radius: 3px;
          text-align: center;
}    </style>

</head>
<body onLoad="brython({debug:1, cache:'browser', static_stdlib_import:true
 })"><!-- background="https://i.imgur.com/8axRcxg.jpg"-->
<div id="pydiv"></div>
</body>
</html>