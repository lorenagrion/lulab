WBOX = "wbox.png"
REDA = "https://i.imgur.com/ZCtrPIP.jpg"


class Proto:
    def __init__(self, by):
        self.by = by
        tela = by.doc["pydiv"]
        dw = by.html.DIV(style=dict(opacity=0))
        iw = by.html.IMG(src=WBOX, width=300)
        dw.bind("mouseenter", self.show_arrow)
        dw.bind("mouseleave", self.hide_arrow)
        tela <= dw
        dw <= iw
        self.div_arrow = da = by.html.DIV(style=dict(opacity=0))
        ia = by.html.IMG(src=REDA, width=300)
        tela <= da
        da <= ia

    def show_arrow(self, ev):
        self.div_arrow.style.opacity = 1
        req = self.by.ajax.ajax()
        req.open('POST', "/register", True)
        req.set_header('content-type', 'application/x-www-form-urlencoded')
        # send data as a dictionary
        req.send({'x': ev.x, 'y': ev.y, "t": self.by.timestamp()})

    def hide_arrow(self, _):
        self.div_arrow.style.opacity = 0


def main(by):
    Proto(by)

if __name__ == '__main__':
    main()
